package net.cubekrowd.chairs;

import com.destroystokyo.paper.MaterialSetTag;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.phys.Vec3;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Tag;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class ChairArmourStand extends ArmorStand {
    public BlockData originalBlockData;
    public Block chairBlock;

    public ChairArmourStand(Block chairBlock, Location loc) {
        super(((CraftWorld) loc.getWorld()).getHandle(), loc.getX(), loc.getY(), loc.getZ());
        var bukkit = (org.bukkit.entity.ArmorStand) getBukkitEntity();
        // make the armour stand interact with the world as little as
        // possible
        bukkit.setInvulnerable(true);
        bukkit.setVisible(false);
        bukkit.setGravity(false);
        bukkit.setCollidable(false);
        bukkit.setCanMove(false);
        // minimal collision box (obstructs block placement)
        bukkit.setSmall(true);
        // never ever save the chair to disk
        bukkit.setPersistent(false);
        // @NOTE(traks) remove hitbox and less possible interactions: players
        // can't pick block the armour stand, can place blocks intersecting the
        // armour stand, etc. Although spectators can still see the armour
        // stand.
        //
        // A possible alternative would be a slime with size 0 (need to modify
        // the synced entity data directly for this), but that only works on
        // servers where mobs are enabled... So you would need to implement a
        // full on fake custom slime entity type to hopefully circumvent that...
        // Marker armour stands are easier
        bukkit.setMarker(true);

        // Hide armour stand hearts when player is mounting it
        var attr = bukkit.getAttribute(Attribute.GENERIC_MAX_HEALTH);
        attr.setBaseValue(0);

        bukkit.setRotation(loc.getYaw(), loc.getPitch());

        this.chairBlock = chairBlock;
        this.originalBlockData = chairBlock.getBlockData();
    }

    public static void removeAll() {
        for (var world : Bukkit.getWorlds()) {
            for (var entity : world.getEntitiesByClass(org.bukkit.entity.ArmorStand.class)) {
                if (((CraftEntity) entity).getHandle() instanceof ChairArmourStand) {
                    ((CraftEntity) entity).getHandle().discard();
                }
            }
        }
    }

    public static Entity putOnChair(Player p, Block chairBlock) {
        Location sitLoc = chairBlock.getLocation();

        if (MaterialSetTag.STAIRS.isTagged(chairBlock.getType())) {
            var stairs = (Stairs) chairBlock.getBlockData();
            if (stairs.getHalf() != Bisected.Half.BOTTOM) {
                // can't sit on upside-down chairs
                return null;
            }

            // @NOTE(traks) set the facing direction of the armour stand. This
            // affects several things:
            //
            //  1. A player's legs can only have an angle of at most 90
            //     degrees with the armour stand's facing direction. When you
            //     turn more while seated, the legs flip 180 degrees.
            //
            //  2. The player body snaps slightly to the facing direction of the
            //     armour stand when the head is facing in a nearby direction.
            sitLoc.setDirection(stairs.getFacing().getDirection().multiply(-1));
            switch (stairs.getShape()) {
            case INNER_LEFT:
            case OUTER_LEFT:
                sitLoc.setYaw(sitLoc.getYaw() - 45);
                break;
            case INNER_RIGHT:
            case OUTER_RIGHT:
                sitLoc.setYaw(sitLoc.getYaw() + 45);
                break;
            }

            sitLoc.add(0.5, 0.51, 0.5);

            // move a bit forward, so the player intersects the stair block less
            if (stairs.getShape() == Stairs.Shape.STRAIGHT) {
                sitLoc.add(sitLoc.getDirection().multiply(0.1));
            } else {
                // corner stairs
                sitLoc.add(sitLoc.getDirection().multiply(0.2));
            }
        } else if (Tag.CAULDRONS.isTagged(chairBlock.getType())) {
            sitLoc.add(0.5, 0.3, 0.5);
            var pLoc = p.getLocation();
            sitLoc.setYaw(pLoc.getYaw());
        } else {
            return null;
        }

        var resInternal = new ChairArmourStand(chairBlock, sitLoc);
        var res = ((CraftWorld) sitLoc.getWorld()).addEntity(resInternal, CreatureSpawnEvent.SpawnReason.CUSTOM);

        if (res != null) {
            // @NOTE(traks) Could force player to face forwards on the chair,
            // but need to teleport player for that, which looks a bit glitchy
            // for clients

            res.addPassenger(p);
        }

        return res;
    }

    public static boolean isChairOccupied(Block chairBlock) {
        for (var entity : chairBlock.getChunk().getEntities()) {
            if (((CraftEntity) entity).getHandle() instanceof ChairArmourStand chair) {
                if (chair.chairBlock.equals(chairBlock)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Vec3 getDismountLocationForPassenger(LivingEntity passenger) {
        var originalType = originalBlockData.getMaterial();
        double deltaUp = 0.05;
        Location res;

        if (MaterialSetTag.STAIRS.isTagged(originalType)) {
            // @TODO(traks) Return location in front of stair if player can
            // stand there? Would need to take half slabs, water, plants, etc.
            // into account.
            res = chairBlock.getLocation().add(0.5, 1 + deltaUp, 0.5);
        } else if (Tag.CAULDRONS.isTagged(originalType)) {
            res = chairBlock.getLocation().add(0.5, 0.25 + deltaUp, 0.5);
        } else {
            // ???
            res = chairBlock.getLocation().add(0.5, 1 + deltaUp, 0.5);
        }

        return new Vec3(res.getX(), res.getY(), res.getZ());
    }

    @Override
    public void tick() {
        super.tick();

        if (!hasExactlyOnePlayerPassenger()) {
            // @NOTE(traks) remove this armour stand entity
            discard();
            return;
        }

        var passenger = passengers.get(0).getBukkitEntity();

        if (!(passenger instanceof Player p)) {
            discard();
            return;
        }

        // @NOTE(traks) the chair is theoretically always in a loaded chunk,
        // so we don't need to worry about sync chunk loads
        var originalType = originalBlockData.getMaterial();

        boolean dismountPlayer = false;

        if (MaterialSetTag.STAIRS.isTagged(originalType)) {
            if (!chairBlock.getBlockData().equals(originalBlockData)) {
                // @NOTE(traks) e.g. straight stair changed into corner
                // stair. Will result in misalignment of sit location
                dismountPlayer = true;
            }
        } else if (Tag.CAULDRONS.isTagged(originalType)) {
            if (!Tag.CAULDRONS.isTagged(chairBlock.getType())) {
                dismountPlayer = true;
            }

            // rotate armour stand to match player facing to make legs less
            // glitchy while seated; see comments above. This is better for
            // cauldrons, because there is no 'natural' facing direction for
            // the armour stand as there is for stair chairs.
            getBukkitEntity().setRotation(p.getLocation().getYaw(), 0);
        } else {
            // no longer a chair block, so dismount player
            dismountPlayer = true;
        }

        if (dismountPlayer) {
            p.leaveVehicle();
        }
    }
}
