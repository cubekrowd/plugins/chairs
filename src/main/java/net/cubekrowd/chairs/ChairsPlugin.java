package net.cubekrowd.chairs;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.java.JavaPlugin;

public class ChairsPlugin extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        ChairArmourStand.removeAll();
    }

    @EventHandler
    public void onChairClick(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        // require main hand so we don't fire twice for same interaction
        if (e.getHand() != EquipmentSlot.HAND) {
            return;
        }

        var p = e.getPlayer();
        var inv = p.getInventory();

        if (p.getGameMode() == GameMode.SPECTATOR) {
            // for some reason interact events fire for spectators, but
            // spectators shouldn't be sitting in chairs
            return;
        }
        if (p.isSneaking()) {
            return;
        }

        // skip if player is holding item
        if (inv.getItemInMainHand().getType() != Material.AIR
                || inv.getItemInOffHand().getType() != Material.AIR) {
            return;
        }

        // ensure chair block is not yet occupied
        var clicked = e.getClickedBlock();
        if (ChairArmourStand.isChairOccupied(clicked)) {
            return;
        }

        var chair = ChairArmourStand.putOnChair(p, clicked);
        if (chair != null) {
            e.setCancelled(true);
        }
    }
}
